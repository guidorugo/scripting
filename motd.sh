#!/bin/bash
#
# Installation:
#
# 1.   vim /etc/ssh/sshd_config
#      PrintMotd no
#
# 2.   vim /etc/pam.d/login
#      # session optional pam_motd.so
#
# 3.   vim /etc/profile
#      /usr/local/bin/dynmotd # Place at the bottom
#
# 4.   Then of course drop this file at
#      /usr/local/bin/dynmotd
#

#
# Handle AWS Sytle volumes
ROOT_AVAIL=`df -Ph / | tail -n 1 | awk '{print $4}' |  tr -d '\n'`
STORE_AVAIL=`df -Ph /store 2>/dev/null | tail -n 1 | awk '{print $4}' |  tr -d '\n'`


USER=`whoami`
HOSTNAME=`uname -n`
IP=`ip -f inet addr show eth0| grep 'inet' | awk '{print $2}' | tr -d '\n'`

MEMORY1=`free -t -m | grep "buffers/cache" | awk '{print $3" MB";}'`
MEMORY2=`free -t -m | grep "Mem" | awk '{print $2" MB";}'`
PSA=`ps -Afl | wc -l`

# time of day
HOUR=$(date +"%H")
if [ $HOUR -lt 12  -a $HOUR -ge 0 ]
then    TIME="morning"
elif [ $HOUR -lt 17 -a $HOUR -ge 12 ]
then    TIME="afternoon"
else
    TIME="evening"
fi

#System uptime
uptime=`cat /proc/uptime | cut -f1 -d.`
upDays=$((uptime/60/60/24))
upHours=$((uptime/60/60%24))
upMins=$((uptime/60%60))
upSecs=$((uptime%60))

#System load
LOAD1=`cat /proc/loadavg | awk {'print $1'}`
LOAD5=`cat /proc/loadavg | awk {'print $2'}`
LOAD15=`cat /proc/loadavg | awk {'print $3'}`

echo "

Good $TIME $USER"

echo "
===========================================================================
 - Hostname............: $HOSTNAME
 - IP Address..........: $IP
 - Release.............: `cat /etc/redhat-release`
 - Users...............: `users | wc -w` user(s) logged on"


echo "===========================================================================
 - You.................: $USER
 - CPU ................: $LOAD1, $LOAD5, $LOAD15 (1, 5, 15 min)
 - Memory used/total...: $MEMORY1 / $MEMORY2
 - Swap in use.........: `free -m | tail -n 1 | awk '{print $3}'` MB
 - Processes...........: $PSA running
 - Uptime..............: $upDays days $upHours hours $upMins minutes $upSecs seconds
 - Disk / .............: $ROOT_AVAIL available"
if [[ ${#STORE_AVAIL} -gt 0 ]]; then
   echo " - Disk /store ........: $STORE_AVAIL available"
fi
 echo "===========================================================================" 