#!/usr/bin/env bash
#
# Evaluate free system memory from Linux based systems based on percentage
# This was forked from Sensu Community Plugins
#
# Requires: bc
#
# Date: 2007-11-12
# Author: Thomas Borger - ESG
# Date: 2012-04-02
# Modified: Norman Harman - norman.harman@mutualmobile.com
# Date: 2013-9-30
# Modified: Mario Harvey - Zumetrics
# Date: 2015-01-10
# Modified Ollie Armstrong <ollie@armstrong.io>
# Date: 2015-10-5
# Modified: Guido Rugo <grugo.zteam@zuora.com> - Maximiliano Esperanza <mesperanza.zteam@zuora.com> - Agustin Recalde <arecalde.zteam@zuora.com> - CHANGED TO /proc/meminfo


# get arguments

# #RED
while getopts 'w:c:hp:d' OPT; do
  case $OPT in
    w)  WARN=$OPTARG;;
    c)  CRIT=$OPTARG;;
    h)  hlp="yes";;
    p)  perform="yes";;
    d)  debug="yes";;
    *)  unknown="yes";;
  esac
done

# Usage
HELP="
    usage: $0 [ -w value -c value -p -h -d ]

        -w --> Warning Percentage < value
        -c --> Critical Percentage < value
        -p --> print out performance data
        -h --> print this help screen
        -d --> print debug info
"

#Print help
if [ "$hlp" = "yes" ]; then
  echo "$HELP"
  exit 0
fi

#Get total memory available on machine
TotalMem=$(cat /proc/meminfo |grep MemTotal | awk '{print $2 }')

#Setting variables
set -o pipefail
FreeMem=$(cat /proc/meminfo |grep MemFree | awk '{print $2 }')
BuffMem=$(cat /proc/meminfo |grep -w Buffers | awk '{print $2 }')
CachMem=$(cat /proc/meminfo |grep -w Cached | awk '{print $2 }')
SlabMem=$(cat /proc/meminfo |grep -w SReclaimable | awk '{print $2 }')
WARN=${WARN:=0}
CRIT=${CRIT:=0}
VERSION=$(rpm -q --queryformat '%{VERSION}' centos-release)

#Determine amount of free memory on the machine
FreePer=$(( $FreeMem + $BuffMem + $CachMem + $SlabMem )) #Avoid using bc
if (( $VERSION >= "7" )); then
  AvMem=$(cat /proc/meminfo |grep -w MemAvailable | awk '{print $2 }')
  FreePer=$AvMem
fi

#Calculating percentage
FreePer=$(( $FreePer * 100 / $TotalMem ))

#Convert it to Mb
MemMB=$(( ( $FreeMem + $BuffMem + $CachMem + $SlabMem ) / 1024 ))

#Get actual memory usage percentage by subtracting free memory percentage from 100
UsedPer=$(( 100 - $FreePer ))

#Debug print
if [ "$debug" = "yes" ]; then 
  echo "FreeMem variable - $FreeMem"
  echo "BuffMem variable - $BuffMem"
  echo "CachMem variable - $CachMem"
  echo "SlabMem variable - $SlabMem"
  if (( $VERSION >= "7" )); then
    echo "AvMem variable - $AvMem"
  fi
  echo "FreePer variable - $FreePer"
  echo "MemMB variable - $MemMB"
  echo "UsedPer variable - $UsedPer"
  echo "WARN variable - $WARN"
  echo "CRIT variable - $CRIT"
fi

if [ "$FreePer" = "" ]; then
  echo "MEM UNKNOWN -"
  exit 3
fi

#Print performance information
if [ "$perform" = "yes" ]; then
  output="system memory usage: $UsedPer% | the amount of free memory=$MemMB;$WARN;$CRIT;0"
else
  output="system memory usage: $UsedPer%"
fi

#Main
if (( $UsedPer >= $CRIT )); then
  echo "MEM CRITICAL - Usage : $UsedPer%"
  exit 2
elif (( $UsedPer >= $WARN )); then
  echo "MEM WARNING - Usage : $UsedPer%"
  exit 1
else
  echo "MEM OK - Usage : $UsedPer%"
  exit 0
fi